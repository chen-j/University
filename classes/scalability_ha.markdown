# Scalability Availability

---

# Refresher!
### GitLab in Parts

---

![inline](images/stack.png)

---

# Scalability

---

> Scalability is the ability of a system to provide throughput in proportion
to available hardware resources

---

> Whether 10 people or 1000 people use GitLab, it's still fast (given you
add enough hardware)

---

# Vertical Scaling

![inline](images/scale_vertical.png)

---

# Horizontal Scaling

![inline](images/scale_horizontal.png)

---

> Scalability is about the means to keeping GitLab fast

---

# Availability

---

> The availability of a system is the percentage of time that it works normally

---

# 9s
---

# 99% available
### Down for 3.65 days per year

---

# 99.999%
### < 5.26 minutes of downtime per year

---

# Availability

- replication
- online deploys
- automatic failover
- mirrors
- clustered services

---

> To achieve high availability, avoid a single point of failure

---

![inline](images/scaled.png)

---

# [fit] Highly available, scalable GitLab

- Highly Available Omnibus packages
- Clustered filesystem
- Documentation on clustering databases
- Using Docker for HA
- ..?
